"""add statistics fields to user table

Revision ID: e130e10dde01
Revises: dd6dbd67841a
Create Date: 2017-09-13 20:04:18.898217

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'e130e10dde01'
down_revision = 'dd6dbd67841a'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('users', sa.Column('current_logged_in', sa.DateTime(), nullable=True))
    op.add_column('users', sa.Column('last_logged_in', sa.DateTime(), nullable=True))
    op.add_column('users', sa.Column('registered_on', sa.DateTime(), nullable=True))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('users', 'registered_on')
    op.drop_column('users', 'last_logged_in')
    op.drop_column('users', 'current_logged_in')
    # ### end Alembic commands ###
