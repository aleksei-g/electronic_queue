from flask import Flask, render_template
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager
from flask_bcrypt import Bcrypt
from flask_mail import Mail
from config import SITE_NAME


app = Flask(__name__)
app.config.from_object('config.DevelopConfig')
db = SQLAlchemy(app)
bcrypt = Bcrypt(app)
mail = Mail(app)

login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = 'users.login'

DEFAULT_TEMPLATE_DATA = {
    'site_name': SITE_NAME,
}

from app.models import User

@login_manager.user_loader
def load_user(user_id):
    return User.query.filter(User.id == int(user_id)).first()

from app.users.views import users_blueprint
from app.queue.views import queue_blueprint

app.register_blueprint(users_blueprint)
app.register_blueprint(queue_blueprint)

from app import models
from .admin import admin

@app.context_processor
def inject_dict_for_all_templates():
    return dict(template_data=DEFAULT_TEMPLATE_DATA)


@app.errorhandler(404)
def page_not_found(error_code):
    return render_template('404.html'), error_code.code


@app.errorhandler(403)
def page_not_found(error_code):
    return render_template('403.html'), error_code.code