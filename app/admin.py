from flask import redirect, url_for, flash, request
from werkzeug.exceptions import HTTPException
from flask_admin import Admin
from flask_admin.contrib.sqla import ModelView
from flask_admin.menu import MenuLink
from flask_login import current_user
from wtforms import PasswordField
from wtforms.validators import Length, EqualTo
from app import app, db
from .models import User


class AuthException(HTTPException):
    def __init__(self, message):
        flash(
            'Для посещения запрашиваемой страницы необходимо авторизоваться '
            'под пользователем с административными правами.',
            'info')
        super().__init__(
            message,
            redirect(url_for('users.login', next=request.path)),
        )


class ModelView(ModelView):
    def check_auth(self):
        return current_user.is_authenticated and current_user.role =='admin'

    def is_accessible(self):
        if not self.check_auth():
            raise AuthException('Not authenticated.')
        return True



class UserView(ModelView):

    def scaffold_form(self):
        form_class = super(UserView, self).scaffold_form()
        form_class.new_password = PasswordField('New password')
        form_class.confirm = PasswordField(
            'Confirm',
            validators=[EqualTo('new_password')],
        )
        return form_class

    def on_model_change(self, form, User, is_created=False):
        if not form.new_password.data == '':
            User.password = form.new_password.data


admin = Admin(
    app,
    name='Электронная очередь (Админка)',
    template_mode='bootstrap3',
)
admin.add_view(UserView(User, db.session))
admin.add_link(MenuLink(name='Вернуться на сайт', category='', url='/'))
