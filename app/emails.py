from flask import render_template, url_for
from flask_mail import Message
from itsdangerous import URLSafeTimedSerializer

from .decorators import async
from app import app, mail
from config import EMAIL_CONFIRM_SALT, PASSWORD_RESET_SALT


@async
def send_async_email(app, msg):
    with app.app_context():
        mail.send(msg)


def send_email(subject, recipients, text_body, html_body):
    if mail.state.username and mail.state.password:
        msg = Message(subject, recipients=recipients)
        msg.body = text_body
        msg.html = html_body
        send_async_email(app, msg)


def registration_notification(user):
    send_email(
        'Электронная очередь: Регистрация на сайте.',
        [user.email],
        render_template('registration_email.txt', user=user),
        render_template('registration_email.html', user=user),
    )


def send_confirmation_email(user_email):
    confirm_serializer = URLSafeTimedSerializer(app.config['SECRET_KEY'])
    confirm_url = url_for(
        'users.confirm_email',
        token=confirm_serializer.dumps(
            user_email,
            salt=EMAIL_CONFIRM_SALT,
        ),
        _external=True,
    )
    send_email(
        'Электронная очередь: Подтверждение e-mail адреса.',
        [user_email],
        render_template(
            'confirmation_email.txt',
            email=user_email,
            confirm_url=confirm_url,
        ),
        render_template(
            'confirmation_email.html',
            email=user_email,
            confirm_url=confirm_url),
    )


def send_password_reset_email(user_email):
    password_reset_serializer = URLSafeTimedSerializer(app.config['SECRET_KEY'])
    password_reset_url = url_for(
        'users.reset_with_token',
        token=password_reset_serializer.dumps(
            user_email,
            salt=PASSWORD_RESET_SALT,
        ),
        _external=True,
    )
    send_email(
        'Электронная очередь: Сброс пароля аккаунта.',
        [user_email],
        render_template(
            'email_password_reset.txt',
            emai=user_email,
            password_reset_url=password_reset_url,
        ),
        render_template(
            'email_password_reset.html',
            email=user_email,
            password_reset_url=password_reset_url),
    )
