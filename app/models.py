from sqlalchemy.ext.hybrid import hybrid_property, hybrid_method
from app import db, bcrypt
from datetime import datetime

# TODO: Convert DateTime fields to timestamp
class User(db.Model):
    __tablename__ = 'users'

    id = db.Column(db.Integer, primary_key=True)
    first_name = db.Column(db.String(100))
    last_name = db.Column(db.String(100))
    email = db.Column(db.String(120), unique=True, nullable=False)
    _password = db.Column(db.LargeBinary(60), nullable=False)
    role = db.Column(db.String, default='user')
    email_confirmation_sent_on = db.Column(db.DateTime, nullable=True)
    email_confirmed = db.Column(db.Boolean, nullable=True, default=False)
    email_confirmed_on = db.Column(db.DateTime, nullable=True)
    registered_on = db.Column(db.DateTime, nullable=True)
    last_logged_in = db.Column(db.DateTime, nullable=True)
    current_logged_in = db.Column(db.DateTime, nullable=True)

    def __init__(self, email=None, plaintext_password='password', role='user',
                 email_confirmation_sent_on=None):
        """Для интеграции с flask-admin указаны значения по умолчанию
        для всех полей функции, т.к. flask-admin не поддерживает передачу
        параметров. Кроме того функция bcrypt.generate_password_hash() требует
        ненулевого значения plaintext_password."""
        self.email = email
        self.password = plaintext_password
        self.role = role
        self.email_confirmation_sent_on = email_confirmation_sent_on
        self.email_confirmed = False
        self.email_confirmed_on = None
        self.registered_on = datetime.now()
        self.last_logged_in = None
        self.current_logged_in = datetime.now()

    @hybrid_property
    def password(self):
        return self._password

    @password.setter
    def set_password(self, plaintext_password):
        self._password = bcrypt.generate_password_hash(plaintext_password)

    @hybrid_method
    def is_correct_password(self, plaintext_password):
        return bcrypt.check_password_hash(self.password, plaintext_password)

    @property
    def is_authenticated(self):
        return True

    @property
    def is_active(self):
        return True

    @property
    def is_anonymous(self):
        return False

    def get_id(self):
        return self.id

    def get_full_username(self):  # pragma: no cover
        if self.first_name is None and self.last_name is None:
            return self.email
        return '{}{}{}'.format(self.first_name,
                               ' ' if self.first_name and self.last_name
                               else '',
                               self.last_name)

    def get_username(self):  # pragma: no cover
        name_items = [self.first_name, self.last_name, self.email]
        return next(item for item in name_items if item is not None)

    def __repr__(self):  # pragma: no cover
        return '<User {}>'.format(self.email)
