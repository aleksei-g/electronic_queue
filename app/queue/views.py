from flask import Blueprint, render_template

from app import app


queue_blueprint = Blueprint('queue', __name__)


@queue_blueprint.route('/')
@queue_blueprint.route('/index')
def index():
    return render_template('index.html')
