from coverage import coverage
cov = coverage(branch=True, omit=['*/electronic_queue-py3.5/*', '*/version/*',
                                  '*/tests/*'])
cov.start()

import unittest
import os
from app import app, db, mail
from config import basedir, PASSWORD_RESET_SALT, EMAIL_CONFIRM_SALT
from app.models import User
from itsdangerous import URLSafeTimedSerializer
from flask import url_for

with app.test_request_context():
    REGISTER_URL = url_for('users.register')
    LOGIN_URL = url_for('users.login')
    LOGOUT_URL = url_for('users.logout')
    USER_PROFILE_URL = url_for('users.user_profile')
    EMAIL_CHANGE_URL = url_for('users.user_email_change')
    RESET_URL = url_for('users.reset')
    PASSWORD_CHANGE_URL = url_for('users.user_password_change')
    ADMIN_URL = url_for('admin.index')



class AppTests(unittest.TestCase):
    def setUp(self):
        app.config['TESTING'] = True
        app.config['DEBUG'] = False
        app.config['WTF_CSRF_ENABLED'] = False
        app.config['SQLALCHEMY_DATABASE_URI'] = \
            'sqlite:///' + os.path.join(basedir, 'test.db')
        self.app = app.test_client()
        db.create_all()

        self.assertEqual(app.debug, False)

    def tearDown(self):
        db.session.remove()
        db.drop_all()

    def register(self, email, password, confirm):
        return self.app.post(
            REGISTER_URL,
            data=dict(email=email, password=password, confirm=confirm),
            follow_redirects=True,
        )

    def login(self, email, password):
        return self.app.post(
            LOGIN_URL,
            data=dict(email=email, password=password),
            follow_redirects=True,
        )

    def reset(self, email):
        return self.app.post(
            RESET_URL,
            data=dict(email=email),
            follow_redirects=True,
        )

    def reset_with_token(self, password_reset_url, password, confirm):
        return self.app.post(
            password_reset_url,
            data=dict(password=password, confirm=confirm),
            follow_redirects=True,
        )

    def test_user(self):
        u1_password = 'my_excellent_password'
        u1 = User(email='test_user@test.ru', plaintext_password=u1_password)
        db.session.add(u1)
        db.session.commit()
        self.assertEqual(u1.is_authenticated, True)
        self.assertEqual(u1.is_active, True)
        self.assertEqual(u1.is_anonymous, False)
        self.assertEqual(u1.id, int(u1.get_id()))
        self.assertEqual(u1.is_correct_password(u1_password), True)

    def test_user_registration_form_displays(self):

        response = self.app.get(REGISTER_URL)
        self.assertEqual(response.status_code, 200)

    def test_valid_user_registration(self):
        self.app.get(REGISTER_URL, follow_redirects=True)
        response = self.register(
            email='test_valid_user@test.ru',
            password='my_excellent_password',
            confirm='my_excellent_password',
        )
        self.assertIn(
            'Регистрация аккаунта прошла успешно!',
            response.data.decode('utf-8'),
        )
        self.app.get('logout', follow_redirects=True)

    def test_valid_user_registration_error(self):
        response = self.register(
            email='registertest.ru',
            password='pass',
            confirm='my_password',
        )
        self.assertIn(
            'Неверный формат адрес электронной почты.',
            response.data.decode('utf-8'),
        )
        self.assertIn(
            'Поле должно содержать от 6 до 40 символов.',
            response.data.decode('utf-8'),
        )
        self.assertIn(
            'Значение должно совпадать с полем',
            response.data.decode('utf-8'),
        )

    def test_duplicate_email_user_registration_error(self):
        self.register(
            email='duplicate@test.ru',
            password='my_excellent_password',
            confirm='my_excellent_password',
        )
        self.app.get('logout', follow_redirects=True)
        response = self.register(
            email='duplicate@test.ru',
            password='my_excellent_password',
            confirm='my_excellent_password',
        )
        self.assertIn(
            'ОШИБКА! E-mail адрес (duplicate@test.ru) уже существует '
            'в базе данных!',
            response.data.decode('utf-8'),
        )

    def test_login_form_displays(self):
        response = self.app.get(LOGIN_URL)
        self.assertEqual(response.status_code, 200)

    def test_valid_login(self):
        self.register(
            email='valid_login@test.ru',
            password='my_excellent_password',
            confirm='my_excellent_password',
        )
        self.app.get('logout', follow_redirects=True)
        self.app.get(LOGIN_URL, follow_redirects=True)
        response = self.login('valid_login@test.ru', 'my_excellent_password')
        self.assertIn(
            'valid_login@test.ru',
            response.data.decode('utf-8'),
        )
        self.app.get('logout', follow_redirects=True)

    def test_invalid_login(self):
        self.app.get(LOGIN_URL, follow_redirects=True)
        response = self.login('invalid_logintest.ru', 'my_excellent_password')
        self.assertIn(
            'Неверный формат адрес электронной почты.',
            response.data.decode('utf-8'),
        )

    def test_get_login_by_current_user(self):
        self.register(
            email='get_login@test.ru',
            password='my_excellent_password',
            confirm='my_excellent_password',
        )
        self.app.get(LOGIN_URL, follow_redirects=True)
        response = self.login('get_login@test.ru', 'my_excellent_password')
        self.assertIn(
            'get_login@test.ru',
            response.data.decode('utf-8'),
        )

    def test_login_without_registering(self):
        self.app.get(LOGIN_URL, follow_redirects=True)
        response = self.login(
            'without_registering@test.ru',
            'my_excellent_password',
        )
        self.assertIn(
            'ОШИБКА! Неверный e-mail или пароль.',
            response.data.decode('utf-8'),
        )

    def test_valid_logout(self):
        self.register(
            email='valid_logout@test.ru',
            password='my_excellent_password',
            confirm='my_excellent_password',
        )
        response = self.app.get(LOGOUT_URL, follow_redirects=True)
        self.assertIn('Вход', response.data.decode('utf-8'))

    def test_invalid_logout_within_being_logged_in(self):
        response = self.app.get(LOGOUT_URL, follow_redirects=True)
        self.assertIn('Вход', response.data.decode('utf-8'))

    def test_user_profile_displyas_for_login_user(self):
        self.register(
            email='user_profile@test.ru',
            password='my_excellent_password',
            confirm='my_excellent_password',
        )
        response = self.app.get(USER_PROFILE_URL, follow_redirects=True)
        self.assertIn('Профиль пользователя', response.data.decode('utf-8'))

    def test_user_profile_without_logging_in(self):
        response = self.app.get(USER_PROFILE_URL)
        self.assertEqual(response.status_code, 302)
        self.assertIn(
            'You should be redirected automatically to target URL:',
            response.data.decode('utf-8'),
        )
        with app.test_request_context():
            go_to_url = url_for(
                'users.login',
                next=url_for('users.user_profile'),
            )
        self.assertIn(go_to_url, response.data.decode('utf-8'))

    def test_admin_page_sucess(self):
        admin_user = User(
            email='admin_page_sucess@test.ru',
            plaintext_password='my_excellent_password',
            role='admin',
        )
        db.session.add(admin_user)
        db.session.commit()
        self.app.get(LOGIN_URL, follow_redirects=True)
        self.login('admin_page_sucess@test.ru', 'my_excellent_password')
        response = self.app.get(ADMIN_URL, follow_redirects=True)
        self.assertIn(
            'Электронная очередь (Админка)',
            response.data.decode('utf-8'),
        )
        self.app.get('logout', follow_redirects=True)

    def test_admin_page_no_acceess(self):
        self.register(
            email='no_acceess@test.ru',
            password='my_excellent_password',
            confirm='my_excellent_password',
        )
        response = self.app.get(ADMIN_URL, follow_redirects=True)
        self.assertIn(
            'Для посещения запрашиваемой страницы необходимо авторизоваться '
            'под пользователем с административными правами.',
            response.data.decode('utf-8'),
        )
        self.app.get('logout', follow_redirects=True)

    def test_admin_page_edit_password(self):
        admin_user = User(
            email='edit_password@test.ru',
            plaintext_password='my_excellent_password',
            role='admin',
        )
        db.session.add(admin_user)
        db.session.commit()
        self.app.get(LOGIN_URL, follow_redirects=True)
        self.login('edit_password@test.ru', 'my_excellent_password')
        with app.test_request_context():
            user_edit_url = '{}{}'.format(
                url_for('admin.index'),
                'user/edit/?id=1',
            )
        response = self.app.post(
            user_edit_url,
            data=dict(
                email='edit_password_user@test.ru',
                new_password='my_excellent_password',
                confirm='my_excellent_password',
            ),
            follow_redirects=True,
        )
        self.assertIn(
            'Record was successfully saved.',
            response.data.decode('utf-8'),
        )
        self.app.get('logout', follow_redirects=True)

    def test_admin_page_edit_email(self):
        admin_user = User(
            email='edit_email@test.ru',
            plaintext_password='my_excellent_password',
            role='admin',
        )
        db.session.add(admin_user)
        db.session.commit()
        self.app.get(LOGIN_URL, follow_redirects=True)
        self.login('edit_email@test.ru', 'my_excellent_password')
        with app.test_request_context():
            email_edit_url = '{}{}'.format(
                url_for('admin.index'),
                'user/edit/?id=1',
            )
        response = self.app.post(
            email_edit_url,
            data=dict(email='new_email@test.ru',),
            follow_redirects=True,
        )
        self.assertIn(
            'Record was successfully saved.',
            response.data.decode('utf-8'),
        )
        self.app.get('logout', follow_redirects=True)

    def test_password_reset_email_form_displays(self):
        response = self.app.get(RESET_URL)
        self.assertEqual(response.status_code, 200)

    def test_password_reset_by_email_not_confirmed(self):
        user = User(
            email='reset_email_not_confirmed@test.ru',
            plaintext_password='my_excellent_password',
            role='user',
        )
        db.session.add(user)
        db.session.commit()
        self.app.get(RESET_URL, follow_redirects=True)
        response = self.reset('reset_email_not_confirmed@test.ru')
        self.assertIn(
            'Чтобы иметь возможность сбросить пароль Ваш e-mail адрес должен '
            'быть подтвержден. Проверьте ваш почтовый ящик на наличие письма '
            'со ссылкой для подтверждения e-mail адреса.',
            response.data.decode('utf-8'),
        )

    def test_password_reset_by_email_confirmed(self):
        user = User(
            email='reset_email_confirmed@test.ru',
            plaintext_password='my_excellent_password',
            role='user',
        )
        db.session.add(user)
        user.email_confirmed = True
        db.session.commit()
        self.app.get(RESET_URL, follow_redirects=True)
        response = self.reset('reset_email_confirmed@test.ru')
        self.assertIn(
            'На указанный e-mail было выслано письмо с дальнейшими '
            'инструкциями.',
            response.data.decode('utf-8'),
        )

    def test_password_reset_by_email_invalid(self):
        self.app.get(RESET_URL, follow_redirects=True)
        response = self.reset('reset_email_invalid@test.ru')
        self.assertIn(
            'Неверный адрес электронной почты.',
            response.data.decode('utf-8'),
        )

    def test_password_reset_with_token_form_displays(self):
        user = User(
            email='reset_with_token@test.ru',
            plaintext_password='my_excellent_password',
            role='user',
        )
        db.session.add(user)
        db.session.commit()
        password_reset_serializer = URLSafeTimedSerializer(
            app.config['SECRET_KEY'])
        with app.test_request_context():
            password_reset_url = url_for(
                'users.reset_with_token',
                token=password_reset_serializer.dumps(
                    user.email,
                    salt=PASSWORD_RESET_SALT,
                ),
            )
        response = self.app.get(password_reset_url, follow_redirects=True)
        self.assertEqual(response.status_code, 200)

    def test_password_reset_with_token_invalid_link(self):
        with app.test_request_context():
            password_reset_url = url_for(
                'users.reset_with_token',
                token='invalid_token',
            )
        response = self.app.get(password_reset_url, follow_redirects=True)
        self.assertIn(
            'Ссылка на сброс пароля неверная или устарела.',
            response.data.decode('utf-8'),
        )

    def test_password_reset_with_token_email_valid(self):
        user = User(
            email='reset_with_token_email_valid@test.ru',
            plaintext_password='my_excellent_password',
            role='user',
        )
        db.session.add(user)
        db.session.commit()
        password_reset_serializer = URLSafeTimedSerializer(
            app.config['SECRET_KEY'])
        with app.test_request_context():
            password_reset_url = url_for(
                'users.reset_with_token',
                token=password_reset_serializer.dumps(
                    user.email,
                    salt=PASSWORD_RESET_SALT,
                ),
            )
        self.app.get(password_reset_url, follow_redirects=True)
        response = self.reset_with_token(
            password_reset_url=password_reset_url,
            password='new_password',
            confirm='new_password',
        )
        self.assertIn(
            'Пароль был успешно изменен!',
            response.data.decode('utf-8'),
        )

    def test_password_reset_with_token_email_invalid(self):
        password_reset_serializer = URLSafeTimedSerializer(
            app.config['SECRET_KEY'])
        with app.test_request_context():
            password_reset_url = url_for(
                'users.reset_with_token',
                token=password_reset_serializer.dumps(
                    'invalid@test.ru',
                    salt=PASSWORD_RESET_SALT,
                ),
            )
        self.app.get(password_reset_url, follow_redirects=True)
        response = self.reset_with_token(
            password_reset_url=password_reset_url,
            password='new_password',
            confirm='new_password',
        )
        self.assertIn(
            'Неверный адрес электронной почты.',
            response.data.decode('utf-8'),
        )

    def test_confirm_email_invalid_link(self):
        confirm_serializer = URLSafeTimedSerializer(app.config['SECRET_KEY'])
        with app.test_request_context():
            confirm_url = url_for(
                'users.confirm_email',
                token=confirm_serializer.dumps(
                    'confirm_email_invalid@test.ru',
                    salt=EMAIL_CONFIRM_SALT),
            )
        response = self.app.get(confirm_url, follow_redirects=True)
        self.assertIn(
            'Ссылка для подтверждения недействительна или устарела.',
            response.data.decode('utf-8'),
        )

    def test_confirm_email_not_confirmed(self):
        user = User(
            email='confirm_not_confirmed_email@test.ru',
            plaintext_password='my_excellent_password',
            role='user',
        )
        db.session.add(user)
        db.session.commit()
        confirm_serializer = URLSafeTimedSerializer(app.config['SECRET_KEY'])
        with app.test_request_context():
            confirm_url = url_for(
                'users.confirm_email',
                token=confirm_serializer.dumps(
                    user.email,
                    salt=EMAIL_CONFIRM_SALT),
            )
        response = self.app.get(confirm_url, follow_redirects=True)
        self.assertIn(
            'Ваш e-mail адрес подтвержден и аккаунт активирован!',
            response.data.decode('utf-8'),
        )

    def test_confirm_email_confirmed(self):
        user = User(
            email='confirm_confirmed_email@test.ru',
            plaintext_password='my_excellent_password',
            role='user',
        )
        db.session.add(user)
        user.email_confirmed = True
        db.session.commit()
        confirm_serializer = URLSafeTimedSerializer(app.config['SECRET_KEY'])
        with app.test_request_context():
            confirm_url = url_for(
                'users.confirm_email',
                token=confirm_serializer.dumps(
                    user.email,
                    salt=EMAIL_CONFIRM_SALT),
            )
        response = self.app.get(confirm_url, follow_redirects=True)
        self.assertIn(
            'Аккаунт уже подтвержден.',
            response.data.decode('utf-8'),
        )

    def test_user_email_change_page(self):
        self.app.get(REGISTER_URL, follow_redirects=True)
        self.register(
            email='user_email_change_page@test.ru',
            password='my_excellent_password',
            confirm='my_excellent_password',
        )
        response = self.app.get(EMAIL_CHANGE_URL)
        self.assertEqual(response.status_code, 200)
        self.assertIn(
            'user_email_change_page@test.ru',
            response.data.decode('utf-8'),
        )

    def test_user_email_change_with_email_valid(self):
        self.app.get(REGISTER_URL, follow_redirects=True)
        self.register(
            email='user_email_change@test.ru',
            password='my_excellent_password',
            confirm='my_excellent_password',
        )
        self.app.post(
            EMAIL_CHANGE_URL,
            data=dict(email='changed_email@test.ru'),
            follow_redirects=True,
        )
        response = self.app.get(USER_PROFILE_URL)
        self.assertEqual(response.status_code, 200)
        self.assertIn('changed_email@test.ru', response.data.decode('utf-8'))
        self.assertNotIn(
            'user_email_change@test.ru@test.ru',
            response.data.decode('utf-8'),
        )
        self.assertIn(
            'Email ардес не подтвержден!',
            response.data.decode('utf-8'),
        )

    def test_user_email_change_with_existing_email(self):
        self.app.get(REGISTER_URL, follow_redirects=True)
        self.register(
            email='user_exist_email_change@test.ru',
            password='my_excellent_password',
            confirm='my_excellent_password',
        )
        response = self.app.post(
            EMAIL_CHANGE_URL,
            data=dict(email='user_exist_email_change@test.ru'),
            follow_redirects=True,
        )
        self.assertEqual(response.status_code, 200)
        self.assertIn(
            'user_exist_email_change@test.ru',
            response.data.decode('utf-8'),
        )
        self.assertIn(
            'Пользователь с указанным email уже существует в базе!',
            response.data.decode('utf-8'),
        )

    def test_user_email_change_without_logging_in(self):
        response = self.app.get(EMAIL_CHANGE_URL)
        self.assertEqual(response.status_code, 302)
        self.assertIn(
            'You should be redirected automatically to target URL:',
            response.data.decode('utf-8'),
        )
        with app.test_request_context():
            go_to_url = url_for(
                'users.login',
                next=url_for('users.user_email_change'),
            )
        self.assertIn(go_to_url, response.data.decode('utf-8'))
        response = self.app.post(
            EMAIL_CHANGE_URL,
            data=dict(email='email_change_fail@test.ru'),
            follow_redirects=True,
        )
        self.assertEqual(response.status_code, 200)
        self.assertIn('Зарегистрируйся!', response.data.decode('utf-8'))

    def test_change_password_page(self):
        self.app.get(REGISTER_URL, follow_redirects=True)
        self.register(
            email='change_password_page@test.ru',
            password='my_excellent_password',
            confirm='my_excellent_password',
        )
        response = self.app.get(PASSWORD_CHANGE_URL)
        self.assertEqual(response.status_code, 200)
        self.assertIn('Новый пароль', response.data.decode('utf-8'))

    def test_change_password(self):
        self.app.get(REGISTER_URL, follow_redirects=True)
        self.register(
            email='change_password_page@test.ru',
            password='my_excellent_password',
            confirm='my_excellent_password',
        )
        response = self.app.post(
            PASSWORD_CHANGE_URL,
            data=dict(
                password='new_excellent_password',
                confirm='new_excellent_password',
            ),
            follow_redirects=True,
        )
        self.assertEqual(response.status_code, 200)
        self.assertIn(
            'Пароль был успешно изменен!',
            response.data.decode('utf-8'),
        )
        self.assertIn('Профиль пользователя', response.data.decode('utf-8'))

    def test_change_password_not_login_in(self):
        response = self.app.get(PASSWORD_CHANGE_URL)
        self.assertEqual(response.status_code, 302)
        self.assertIn(
            'You should be redirected automatically to target URL:',
            response.data.decode('utf-8'),
        )
        response = self.app.post(
            PASSWORD_CHANGE_URL,
            data=dict(
                password='new_excellent_password',
                confirm='new_excellent_password',
            ),
            follow_redirects=True,
        )
        self.assertEqual(response.status_code, 200)
        self.assertIn('Зарегистрируйся!', response.data.decode('utf-8'))


if __name__ == '__main__':
    try:
        unittest.main()
    except:
        pass
    cov.stop()
    cov.save()
    print('\n\nCoverage Report:\n')
    cov.report()
    print('HTML version: ' + os.path.join(basedir, 'tmp/coverage/index.html'))
    cov.html_report(directory=os.path.join(basedir, 'tmp/coverage/'))
    cov.erase()
