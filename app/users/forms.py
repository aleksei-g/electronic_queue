from flask_wtf import FlaskForm as Form
from wtforms import StringField, PasswordField
from wtforms.validators import DataRequired, Length, EqualTo, Email

valid_errors = {'DataRequired': 'Это поле должно быть заполнено.',
                'Email': 'Неверный формат адрес электронной почты.',
                'Length:6-40': 'Поле должно содержать от 6 до 40 символов.',
                'EqualTo': 'Значение должно совпадать с полем "Пароль".'}


class RegisterForm(Form):
    email = StringField(
        'E-mail',
        validators=[DataRequired(message=valid_errors['DataRequired']),
                    Email(message=valid_errors['Email']),
                    Length(min=6, max=40, message=valid_errors['Length:6-40']),
                    ],
    )
    password = PasswordField(
        'Пароль',
        validators=[DataRequired(message=valid_errors['DataRequired']),
                    Length(min=6, max=40, message=valid_errors['Length:6-40']),
                    ],
    )
    confirm = PasswordField(
        'Повторите пароль',
        validators=[DataRequired(message=valid_errors['DataRequired']),
                    EqualTo('password', message=valid_errors['EqualTo']),
                    ],
    )


class LoginForm(Form):
    email = StringField(
        'E-mail',
        validators=[DataRequired(message=valid_errors['DataRequired']),
                    Email(message=valid_errors['Email']),
                    Length(min=6, max=40, message=valid_errors['Length:6-40']),
                    ],
    )
    password = PasswordField('Пароль', validators=[DataRequired()])


class EmailForm(Form):
    email = StringField(
        'E-mail',
        validators=[DataRequired(message=valid_errors['DataRequired']),
                    Email(message=valid_errors['Email']),
                    Length(min=6, max=40, message=valid_errors['Length:6-40']),
                    ],
    )


class PasswordForm(Form):
    password = PasswordField(
        'Пароль',
        validators=[DataRequired(message=valid_errors['DataRequired']),
                    Length(min=6, max=40, message=valid_errors['Length:6-40']),
                    ],
    )
    confirm = PasswordField(
        'Повторите пароль',
        validators=[DataRequired(message=valid_errors['DataRequired']),
                    EqualTo('password', message=valid_errors['EqualTo']),
                    ],
    )
