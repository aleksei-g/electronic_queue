from flask import render_template, Blueprint, request, flash, redirect, url_for
from flask_login import login_user, current_user, login_required, logout_user
from sqlalchemy.exc import IntegrityError
from itsdangerous import URLSafeTimedSerializer
from datetime import datetime

from app import app, db, DEFAULT_TEMPLATE_DATA
from app.models import User
from app.users.forms import RegisterForm, LoginForm, EmailForm, PasswordForm
from app.emails import registration_notification, send_confirmation_email, \
    send_password_reset_email
from config import EMAIL_CONFIRM_SALT, PASSWORD_RESET_SALT
users_blueprint = Blueprint('users', __name__)


@users_blueprint.route('/register', methods=['GET', 'POST'])
def register():
    form = RegisterForm(request.form)
    if request.method == 'POST':
        if form.validate_on_submit():
            try:
                new_user = User(form.email.data, form.password.data)
                db.session.add(new_user)
                db.session.commit()
                login_user(new_user)
                send_confirmation_email(new_user.email)
                flash(
                    'Регистрация аккаунта прошла успешно! На ваш электронный'
                    'адрес отравлено письмо со ссылкой для активации аккаунта.',
                    'success',
                )
                return redirect(url_for('queue.index'))
            except IntegrityError:
                db.session.rollback()
                flash(
                    'ОШИБКА! E-mail адрес ({}) уже существует в базе данных!'
                    .format(form.email.data),
                    'error',
                )
    return render_template('register.html',
                           form=form,
                           template_data={**DEFAULT_TEMPLATE_DATA,
                                          **{'title': 'Регистрация'}},
                           )


@users_blueprint.route('/confirm/<token>')
def confirm_email(token):
    try:
        confirm_serializer = URLSafeTimedSerializer(app.config['SECRET_KEY'])
        email = confirm_serializer.loads(
            token,
            salt=EMAIL_CONFIRM_SALT,
            max_age=3600,
        )
        user = User.query.filter_by(email=email).first()
        if user.email_confirmed:
            flash('Аккаунт уже подтвержден.', 'info')
        else:
            user.email_confirmed = True
            user.email_confirmed_on = datetime.utcnow()
            db.session.add(user)
            db.session.commit()
            flash(
                'Ваш e-mail адрес подтвержден и аккаунт активирован!',
                'success',
            )
        return redirect(url_for('queue.index'))
    except:
        flash(
            'Ссылка для подтверждения недействительна или устарела.',
            'error',
        )
        return redirect(url_for('users.login'))


@users_blueprint.route('/reset', methods=['GET', 'POST'])
def reset():
    form = EmailForm()
    if form.validate_on_submit():
        try:
            user = User.query.filter_by(email=form.email.data).first_or_404()
        except:
            flash('Неверный адрес электронной почты.', 'error')
            return render_template('password_reset_email.html', form=form)
        if user.email_confirmed:
            send_password_reset_email(user.email)
            flash(
                'На указанный e-mail было выслано письмо с дальнейшими '
                'инструкциями.',
                'success',
            )
        else:
            flash(
                'Чтобы иметь возможность сбросить пароль Ваш e-mail адрес '
                'должен быть подтвержден. Проверьте ваш почтовый ящик на '
                'наличие письма со ссылкой для подтверждения e-mail адреса.',
                'error',
            )
        return redirect(url_for('users.login'))
    return render_template('password_reset_email.html',
                           form=form,
                           template_data={**DEFAULT_TEMPLATE_DATA,
                                          **{'title': 'Сброс пароля'}},
                           )


@users_blueprint.route('/reset/<token>', methods=['GET', 'POST'])
def reset_with_token(token):
    try:
        password_reset_serializer = URLSafeTimedSerializer(
            app.config['SECRET_KEY'],
        )
        email = password_reset_serializer.loads(
            token,
            salt=PASSWORD_RESET_SALT,
            max_age=3600,
        )
    except:
        flash('Ссылка на сброс пароля неверная или устарела.', 'error')
        return redirect(url_for('users.login'))
    form = PasswordForm()
    if form.validate_on_submit():
        try:
            user = User.query.filter_by(email=email).first_or_404()
        except:
            flash('Неверный адрес электронной почты.', 'error')
            return redirect(url_for('users.login'))
        user.password = form.password.data
        db.session.add(user)
        db.session.commit()
        flash('Пароль был успешно изменен!', 'success')
        return redirect(url_for('users.login'))
    return render_template(
        'password_change.html',
        form=form,
        form_action=url_for('users.reset_with_token', token=token),
        template_data={**DEFAULT_TEMPLATE_DATA,
                       **{'title': 'Изменение пароля'}},
    )


@users_blueprint.route('/password_change', methods=['GET', 'POST'])
@login_required
def user_password_change():
    form = PasswordForm()
    if request.method == 'POST':
        if form.validate_on_submit():
            user = current_user
            user.password = form.password.data
            db.session.add(user)
            db.session.commit()
            flash('Пароль был успешно изменен!', 'success')
            return redirect(url_for('users.user_profile'))
    return render_template(
        'password_change.html',
        form=form,
        form_action=url_for('users.user_password_change'),
        template_data={**DEFAULT_TEMPLATE_DATA,
                       **{'title': 'Изменение пароля'}},
    )


@users_blueprint.route('/email_change', methods=['GET', 'POST'])
@login_required
def user_email_change():
    form = EmailForm()
    if request.method == 'POST':
        if form.validate_on_submit():
            try:
                user_check = User.query.filter_by(email=form.email.data).first()
                if user_check is None:
                    user = current_user
                    user.email = form.email.data
                    user.email_confirmed = False
                    user.email_confirmed_on = None
                    user.email_confirmation_sent_on = datetime.now()
                    db.session.add(user)
                    db.session.commit()
                    send_confirmation_email(user.email)
                    flash(
                        'Email адрес изменен!  Пожалуйста, подтвердите ваш '
                        'новый email адрес (письмо с интсрукциями отправлено '
                        'по почте).',
                        'success',
                    )
                    return redirect(url_for('users.user_profile'))
                else:
                    flash(
                        'Пользователь с указанным email уже существует в базе!',
                        'error',
                    )
            except IntegrityError:
                flash(
                    'Ошибка! Пользователь с указанным email уже существует в '
                    'базе!',
                    'error',
                )
    return render_template(
        'email_change.html',
        form=form,
        template_data={**DEFAULT_TEMPLATE_DATA,
                       **{'title': 'Изменение Email'}},
    )


@users_blueprint.route('/login', methods=['GET', 'POST'])
def login():
    if current_user is not None and current_user.is_authenticated:
        return redirect(url_for('queue.index'))
    form = LoginForm(request.form)
    if request.method == 'POST':
        if form.validate_on_submit():
            user = User.query.filter_by(email=form.email.data).first()
            if user and user.is_correct_password(form.password.data):
                user.last_logged_in = user.current_logged_in
                user.current_logged_in = datetime.utcnow()
                db.session.add(user)
                db.session.commit()
                login_user(user)
                return redirect(
                    request.args.get('next') or url_for('queue.index')
                )
            else:
                flash('ОШИБКА! Неверный e-mail или пароль.', 'error')
    return render_template(
        'login.html',
        form=form,
        template_data={**DEFAULT_TEMPLATE_DATA,
                       **{'title': 'Вход'}},
    )


@users_blueprint.route('/user_profile')
@login_required
def user_profile():
    return render_template(
        'user_profile.html',
        template_data={**DEFAULT_TEMPLATE_DATA,
                       **{'title': 'Профиль пользователя'}},
    )


@users_blueprint.route('/resend_confirmation')
@login_required
def resend_email_confirmation():  # pragma: no cover
    try:
        send_confirmation_email(current_user.email)
        flash(
            'На ваш электронный адрес отравлено письмо со ссылкой для '
            'активации аккаунта.',
            'success',
        )
    except IntegrityError:
        flash(
            'Ошибка!. Не удалось отправить письмо с подверждением на ваш '
            'e-mail адрес.',
            'error',
        )
    return redirect(url_for('users.user_profile'))


@users_blueprint.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('queue.index'))
