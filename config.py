import os
basedir = os.path.abspath(os.path.dirname(__file__))
EMAIL_CONFIRM_SALT = 'sdgshrtughbnmhjlr67r56bvcbe453'
PASSWORD_RESET_SALT = 'djfsdjfJHHdjfae4u9fjdfjxvbH>.f'
SITE_NAME = os.getenv('SITE_NAME', 'Электронная очередь')

class Config(object):
    DEBUG = False
    CSRF_ENABLED = True
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    WTF_CSRF_SECRET_KEY = 'kjblkjhffgcgkjh;j7i6'
    SECRET_KEY = 'sdfourljdff;lu44o5jl;kdfj84'
    SQLALCHEMY_DATABASE_URI = os.getenv('DATABASE_URL') or \
                              'sqlite:///' + os.path.join(basedir, 'app.db')
    # Bcrypt algorithm hashing rounds
    BCRYPT_LOG_ROUNDS = 12  # 12 - default
    #E-mail params
    MAIL_SERVER = 'smtp.mail.ru'
    MAIL_PORT = 465
    MAIL_USE_TLS = False
    MAIL_USE_SSL = True
    MAIL_USERNAME = os.getenv('MAIL_USERNAME')
    MAIL_PASSWORD = os.getenv('MAIL_PASSWORD')
    MAIL_DEFAULT_SENDER = os.getenv('MAIL_USERNAME')


class ProductionConfig(Config):
    DEBUG = False


class DevelopConfig(Config):
    DEBUG = True
    ASSETS_DEBUG = True