import argparse
from getpass import getpass
from sqlalchemy.exc import IntegrityError
from app import db
from app.models import User


def create_parser():
    parser = argparse.ArgumentParser(
        description='Скрипт для создания в БД пользователя с административными '
                    'правами.',
    )
    parser.add_argument(
        '-e',
        '--email',
        metavar='E-MAIL',
        help='E-mail адрес администратора.',
    )
    parser.add_argument(
        '-p',
        '--password',
        metavar='PASSWORD',
        help='Пароль администратора.',
    )
    return parser

if __name__ == '__main__':
    parser = create_parser()
    namespace = parser.parse_args()
    email = namespace.email or input('e-mail: ')
    password = namespace.password or getpass('password: ')
    if email and password:
        try:
            admin_user = User(email=email,
                              plaintext_password=password,
                              role='admin',
                              )
            db.session.add(admin_user)
            db.session.commit()
            print(
                'User "{}" with role "admin" created successfully!'
                .format(email)
            )
        except IntegrityError:
            db.session.rollback()
            print(
                'ОШИБКА! E-mail адрес ({}) уже существует в базе данных.'
                .format(email)
            )
    else:
        print('ОШИБКА! Не были переданы параметры "e-mail" и "password"!')
